// ========================================
// CS570 Xinlei Cao
// Tic-Tac-Toe EXTENDED
// smallest built version
//
// the getANumber() function referenced the type safe method from :
// Zaita (user name
// http://www.cplusplus.com/forum/articles/6046/
// ========================================

// head files
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <cctype>
#include <ctime>
// using name space
using std::cin;
using std::cout;
using std::endl;

// ==============================================
// used to store action sequence
// ==============================================
class Replay
{
public:
	int action[2];// 0 row, 1 column
	// which player's action 1 - 26
	int who;
	// clock ticks since game begin
	int ticks;
	static int ticksFromFile;
	// point to head
	static class Replay *head;
	// point to next
	class Replay *next;
	Replay():who(0),ticks(0){ action[0] = 0; action[1] = 0; next = NULL; };
	~Replay(){ delete next; next = NULL; };
};
class Replay *Replay::head = NULL;
int Replay::ticksFromFile = 0;
// ===============================================
// VARIABLES
// ===============================================
Replay *mReplay;

int howManyPlayers;// how many players are playing 2 - 52
int scaleOfBoard;// the scale of the board
int winSequence;// how many in a line

//used to memerise the chess board , update the chess board with this array , row|column
int **chessBoard;
// player1 to playerN
int whichPlayersTurn;
// if game is end
bool gameOver;
// who win, -1 no result, 0 draw, 1-n, player 1 to playerN
int whichPlayerWin;

bool saveGame;// true save
bool exitGame;// true exit
// action , each action contains a row number and a column number
int action[2];// 0 - row, 1 - column
// ===============================================
// FUNCTIONS
// ===============================================
// init a empty tic-tac-toe board
void initChessBoard( int **(&array), int scale )
{
	array = new int *[scale];
	for( int i = 0; i != scale; i ++ )
		array[i] = new int [scale];
	for( int i = 0; i != scale; i ++ )
		for( int j = 0; j != scale; j ++ )
			array[i][j] = 0;
}
int **initChessBoard( int scale )
{
	int **board;
	board = new int *[scale];
	for( int i = 0; i != scale; i ++ )
		board[i] = new int [scale];
	for( int i = 0; i != scale; i ++ )
		for( int j = 0; j != scale; j ++ )
			board[i][j] = 0;
	return board;
}

// check if the setting is legal
bool isGameSettingLegal( int inNumOfPlayers, int inScaleOfBoard, int inwinSequence )
{
	if( inNumOfPlayers > 52 || inNumOfPlayers < 2 )
	{
		std::cerr<<"error : should be at least 2 players and no more than 52 players.";
		std::cerr<<"\nerror : winning is not possible.\n";
		return false;
	}
	if( inwinSequence > inScaleOfBoard )
	{
		std::cerr<<"error : count of win sequence should not be bigger than the scale of the board.";
		std::cerr<<"\nerror : winning is not possible.\n";
		return false;
	}
	int max = inScaleOfBoard * inScaleOfBoard;// number of grids
	int sum = inwinSequence + ( inNumOfPlayers - 1 )*( inwinSequence - 1 );// the least number of grids needed to end a game
	if( sum > max )
	{
		std::cerr<<"error : there is not enough space for any player to put "<<inwinSequence<<" respect marks.";
		std::cerr<<"\nerror : winning is not possible.\n";
		return false;
	}
	return true;
}


// *************************************************** //
// the type safe method to get a number from input     //
// referenced from :                                   //
// Zaita (2292) 	Dec 1, 2008 at 8:15pm              //
// http://www.cplusplus.com/forum/articles/6046/       //
// *************************************************** //
int getANumber()
{
	std::string input;
	int mNumber;
	while(true)
	{
		std::getline(cin, input);
		std::stringstream myStream(input);
		if (myStream >> mNumber)
			return mNumber;
		else
			std::cerr<<"error : invalid input, please input again.\n";
	}
}
bool load();
void initGame()
{
	cout<<"\n";
	cout<<"============================\n";
	cout<<"====      New Game      ====\n";
	cout<<"============================\n";

	do
	{
		cout<<"please input correct settings for the game."<<endl;
		cout<<"Number of players : ";
		howManyPlayers = getANumber();
		cout<<"Scale of the board : ";
		scaleOfBoard = getANumber();
		cout<<"count of win sequence : ";
		winSequence = getANumber();
	}while( !isGameSettingLegal( howManyPlayers, scaleOfBoard, winSequence ));
	//chessBoard = initChessBoard( scaleOfBoard );
	initChessBoard( chessBoard, scaleOfBoard );
	// player1's turn
	whichPlayersTurn = 1;
}
// set up the game
void setGame()
{
	int choice = 0;
	do
	{
		cout<<"\nWould you like to load game ?\n";
		cout<<"1.Yes, resume a saved game.\n";
		cout<<"2.No, begin a new game."<<endl;
		choice = getANumber();
	}while( choice != 1 && choice != 2 );
	if( choice == 1 )
	{// load
		if( !load() )
			initGame();
	}
	else
	{// new game
		initGame();
	}
	// the innitial game loop state
	whichPlayerWin = -1;
	gameOver = false;
	saveGame = false;
	exitGame = false;
}
// check if the chess board is in a correct state
bool checkChessBoradStatus()
{
	bool status = true;// true when valid

	int *players = new int [howManyPlayers];// count for each player's actions
	for( int i = 0; i != howManyPlayers; i ++ )
		players[i] = 0;
	for( int i = 0; i != scaleOfBoard; i ++ )
	{
		for( int j = 0; j != scaleOfBoard; j ++ )
		{
			if( chessBoard[i][j] < 0 || chessBoard[i][j] > howManyPlayers )
				status = false;
			else
			{
				// count actions of each players
				if( chessBoard[i][j] != 0 )
					players[chessBoard[i][j]-1]++;
			}	
		}
	}
	int max = players[0];
	for( int i = 0; i != howManyPlayers; i ++ )
	{
		if( max < players[i] )
			max = players[i];
	}
	for( int i = 0; i != howManyPlayers; i ++ )
	{
		if( ( max - players[i] ) > 1 )
			status = false;
	}

	return status;
}

// reset the chess board to innitial state
void resetChessBoard()
{
	for( int i = 0; i != scaleOfBoard; i ++ )
	{
		for( int j = 0; j != scaleOfBoard; j ++ )
		{
			chessBoard[i][j] = 0;
		}
	}
}


// check if the action is legal
bool isActionLegal( int inputRow, int inputColumn )
{	
	// if the row number is correct
	if( inputRow > scaleOfBoard || inputRow < 1 )
	{
		std::cerr<<"error : row number is incorrect.";
		return false;
	}
	else if( inputColumn > scaleOfBoard || inputColumn < 1 )
	{
		std::cerr<<"error : column number is incorrect.";
		return false;
	}
	else
	{
		// if there is already a piece at that position
		if( chessBoard[ inputRow - 1 ][ inputColumn - 1 ] == 0 )
			return true;
		else
		{
			std::cerr<<"there is already a piece at the position.";
			return false;
		}
	}
}
bool isActionLegal( int (&a)[2] )
{
	return isActionLegal( a[0], a[1] );
}
// update the chess board
// whosAction : Player1 = 1, Player2 = 2 ....
bool updateChessBoard( int inputRow, int inputColumn , int whosAction )
{
	// check if the chess board is already in wrong status
	if( checkChessBoradStatus() == false )
	{
		// error
		std::cerr<<"error : the chess board is not in a correct status, it will not be updated.";
		return false;
	}
	else
	{
		if( whosAction >howManyPlayers || whosAction < 1 )
		{
			// error
			std::cerr<<"error : the action value can only be 1 - 52.";
			return false;
		}
		else
		{
			if( isActionLegal( inputRow, inputColumn ) )
			{
				// update the chess board
				chessBoard[ inputRow - 1 ][ inputColumn - 1 ] = whosAction;
				return true;
			}
			else
			{
				// illegal action
				std::cerr<<"illegal action.";
				return false;
			}
		}
	}
}
bool updateChessBoard( int (&a)[2], int whosAction )
{
	return updateChessBoard( a[0], a[1], whosAction );
}
bool updateChessBoard()
{
	return updateChessBoard( action, whichPlayersTurn );
}
// convert numbers to letters
char convertToChar( int num )
{
	char marks[53];
	marks[0] = ' ';
	marks[1] = 'X';
	marks[2] = 'O';
	for( int i = 0; i != 14; i ++ )
		marks[i+3] = (char)( i + 'A' );
	for( int i = 0; i != 8; i ++ )
		marks[i+17] = (char)( i + 'P');
	marks[25] = 'Y';
	marks[26] = 'Z';
	for( int i = 0; i != 26; i ++ )
		marks[i+27] = (char)( i + 'a');
	return marks[num];
}
// print the chess board in a acceptable way to the screen
void printChessBoard()
{
	// print the chess board
	cout<<"\n\nChess Borad :"<<endl;
	for( int i = 0; i != scaleOfBoard; i ++ )
	{// each row
		// print numbers
		if( i == 0 )
		{// first row
			for( int j = 0; j != scaleOfBoard; j ++ )
			{// each column
				if( j == 0 )
					cout<<"  ";
				if( j + 1 < 10 )
					cout<<" "<<j+1<<" ";
				else
					cout<<" "<<j+1;
				if( j != scaleOfBoard -1 )
					cout<<" ";
			}
			cout<<"\n";
		}
		// print grids
		else
		{// other rows
			for( int j = 0; j != scaleOfBoard; j ++ )
			{// each column
				if( j == 0 )
					cout<<"  ";
				cout<<"---";
				if( j != scaleOfBoard -1 )
					cout<<"+";
			}
			cout<<"\n";
		}
		// print marks
		for( int j = 0; j != scaleOfBoard; j ++ )
		{
			if( j == 0 )
				if( i + 1 < 10 )
					cout<<i + 1<<" ";
				else
					cout<<i + 1;
			cout<<" "<<convertToChar(chessBoard[i][j])<<" ";
			if( j != scaleOfBoard - 1 )
				cout<<"|";
		}
		cout<<"\n";
	}
	cout<<"\n\n"<<endl;
}

// get input from the keyboard , interaction
void keyboard()
{
	int menu;// 1.continue, 2.save, 3.exit
	// save/exit/continue
	cout<<"========================\n";
	cout<<"         Menu           \n";
	cout<<"Input a number to select\n";
	cout<<"1.continue\n";
	cout<<"2.save\n";
	cout<<"3.exit\n";
	cout<<"========================\n";
	do
	{
		cout<<"you can only input 1,2,3 as choice\n";
		menu = getANumber();
	}while( menu != 1 && menu != 2 && menu != 3 );
	if( menu == 1 )
	{// continue
		// show it is which player's turn
		cout<<"Player"<<whichPlayersTurn<<"'s Turn."<<endl;
		// get input from keyboard
		do
		{
			cout<<"please input your action....."<<endl;
			cout<<"row : ";
			action[0] = getANumber();
			cout<<"colum : ";
			action[1] = getANumber();

		} while ( isActionLegal( action ) == false );
	}
	else if( menu == 2 )
	{// save
		saveGame = true;
	}
	else if( menu == 3 )
	{// exit
		exitGame = true;
	}
}
bool saveReplay( std::string name );
bool save()
{
	if( saveReplay( std::string( "temp.dat" )))
	{
		std::ofstream mSaveData;
		mSaveData.close();
		mSaveData.clear();
		mSaveData.open( "save.dat" );
		if( mSaveData )
		{
			mSaveData<<"<howManyPlayers>+"<<howManyPlayers<<"-\n";
			mSaveData<<"<scaleOfBoard>+"<<scaleOfBoard<<"-\n";
			mSaveData<<"<winSequence>+"<<winSequence<<"-\n";
			mSaveData<<"<whichPlayersTurn>+"<<whichPlayersTurn<<"-\n";
			mSaveData<<"<chessBoard>+\n";
			for( int i = 0; i != scaleOfBoard; i ++ )
			{
				for( int j = 0; j != scaleOfBoard; j ++ )
				{
					mSaveData<<chessBoard[i][j]<<",";
				}
				mSaveData<<"|\n";
			}
			mSaveData<<"-\n";
			mSaveData.close();
			cout<<"game saved, exiting......"<<endl;
			exitGame = true;

			return true;
		}
		else
		{
			std::cerr<<"error : can not save game.";
			return false;
		}
	}
	else
	{
		std::cerr<<"error : can not save game.";
		return false;
	}
}
bool loadReplay( std::string name );
bool load()
{
	// load replay
	if( loadReplay( std::string("temp.dat")))
	{
		mReplay = Replay::head;
		while( mReplay->next != NULL )
		{
			mReplay = mReplay->next;
		}
		Replay::ticksFromFile = mReplay->ticks;


		// load saved data
		cout<<"\nLoading Game....";
		std::ifstream mLoadData;
		mLoadData.close();
		mLoadData.clear();
		mLoadData.open( "save.dat" );
		if( mLoadData )
		{
			// read conditions
			char content;
			std::string type;
			bool readType = false;
			
			int data = 0;
			int **array = NULL;
			int i = 0;
			int j = 0;
			bool readData = false;
			
			// reading
			while( !mLoadData.eof() )
			{
				// keep read from file
				content = mLoadData.get();
				
				if( content == '<' )
				{// read type
					readType = true;
					// reset type
					type.clear();
				}
				else if( content == '>' )
				{
					// complete read type
					readType = false;
				}
				else if( readType == true )
				{
					// add content to string
					type += content;
				}
		
				if( content == '+' )
				{// read game data
					readData = true;
					// reset data
					data = 0;
				
					if( type == "chessBoard" )
					{
						initChessBoard( array, scaleOfBoard );
						i = 0;
						j = 0;
					}
				}
				else if( content == '-' )
				{// complete reading game data
					readData = false;
					// store data into game
					if( type == "howManyPlayers" )
						howManyPlayers = data;
					else if( type == "scaleOfBoard" )
						scaleOfBoard = data;
					else if( type == "winSequence" )
						winSequence = data;
					else if( type == "whichPlayersTurn" )
						whichPlayersTurn = data;
					else if( type == "chessBoard" )
						chessBoard = array;
					else
						std::cerr<<"error : no such type.";
				}
				else if( readData == true )
				{
					if( isdigit( content ) )
					{
						// integers
						data = 10 * data + ( content - '0');
					}
					if( type == "chessBoard" )
					{// array
						if( content == ',' )
						{
							array[i][j] = data;
							data = 0;
							j ++;
						}
						if( content == '|' )
						{
							j = 0;
							i ++;
						}
					}
				}
			}
			mLoadData.close();
			cout<<"\nLoading Complete.";
			return true;
		}
		else
		{
			std::cerr<<"error : can not load game.";
			return false;
		}
	}
	else
	{
		std::cerr<<"error : can not load game.";
		return false;
	}

}
bool isEndLegal()
{
	if( whichPlayerWin > howManyPlayers || whichPlayerWin < -1 )
	{
		// illegal
		std::cerr<<"( error: result can only be one of 1-26 players win or 0 end in a draw or -1 no result. )";
		return false;
	}
	else 
		return true;
}

// judge is one side has win the game
bool isGameOver()
{
	if( gameOver == true )
		return gameOver;

	int *consecutiveMarksOfEachPlayers = new int [howManyPlayers];// only care about player 1- 26
	int prevMark;// previous mark 0- 26
	// check horizontal rows
	if( winSequence == 1 )
	{
		gameOver = true;
		whichPlayerWin = 1;
		return gameOver;
	}
	for( int i = 0; i != scaleOfBoard; i ++ )
	{// for each horizontal rows
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int j = 0; j != scaleOfBoard; j ++ )
		{// for each grid
			if( chessBoard[i][j] > 0 && chessBoard[i][j] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[i][j] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[i][j];
		}
	}
	// check vertical rows
	for( int j = 0; j != scaleOfBoard; j ++ )
	{// for each vertical rows
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int i = 0; i != scaleOfBoard; i ++ )
		{// for each grid
			if( chessBoard[i][j] > 0 && chessBoard[i][j] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[i][j] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[i][j]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[i][j];
		}
	}
	// check diagnal rows
	for( int j = 0; j != scaleOfBoard; j ++ )
	{// \\\\\\\\\\....
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int n = 0; j+n != scaleOfBoard; n ++ )
		{// for each grid
			
			if( chessBoard[0+n][j+n] > 0 && chessBoard[0+n][j+n] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[0+n][j+n] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[0+n][j+n]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[0+n][j+n]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[0+n][j+n]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[0+n][j+n]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[0+n][j+n];
		}
			
	}
	for( int i = 0; i != scaleOfBoard; i ++ )
	{//    \
	 //    \
	 //    \
	 //    \
	 //    .
	 //    .
	 //    .
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int n = 0; i+n != scaleOfBoard; n ++ )
		{// for each grid
			if( chessBoard[i+n][0+n] > 0 && chessBoard[i+n][0+n] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[i+n][0+n] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[i+n][0+n]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[i+n][0+n]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[i+n][0+n]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[i+n][0+n]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[i+n][0+n];
		}
	}
	for( int j = 0; j != scaleOfBoard; j ++ )
	{// //////////.....
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int n = 0; j-n != -1; n ++ )
		{// for each grid
			
			if( chessBoard[0+n][j-n] > 0 && chessBoard[0+n][j-n] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[0+n][j-n] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[0+n][j-n]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[0+n][j-n]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[0+n][j-n]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[0+n][j-n]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[0+n][j-n];
		}
	}
	for( int i = 0; i != scaleOfBoard; i ++ )
	{//    /
	 //    /
	 //    /
	 //    /
	 //    .
	 //    .
	 //    .
		// clear counts of marks for every player
		for( int k = 0; k != howManyPlayers; k ++ )
			consecutiveMarksOfEachPlayers[k] = 0;
		// clear storage of previous mark
		prevMark = 0;
		for( int n = 0; i+n != scaleOfBoard; n ++ )
		{// for each grid
			
			if( chessBoard[i+n][scaleOfBoard-1-n] > 0 && chessBoard[i+n][scaleOfBoard-1-n] <= howManyPlayers )
			{// do not care about 0, since it means the grid is empty
				// check if the marks is consecutive
				if( chessBoard[i+n][scaleOfBoard-1-n] == prevMark )
				{// if consecutive
					// count of this mark increase 1
					consecutiveMarksOfEachPlayers[chessBoard[i+n][scaleOfBoard-1-n]-1] ++;// - 1 because only care about 26 players
					// check the number of the marks
					// if the consecutive marks are no less than the win consequence
					if( consecutiveMarksOfEachPlayers[chessBoard[i+n][scaleOfBoard-1-n]-1] >= winSequence )
					{// this player win
						gameOver = true;
						whichPlayerWin = prevMark;
						return gameOver;
					}
				}
				else
				{// if not consecutive
					if( consecutiveMarksOfEachPlayers[chessBoard[i+n][scaleOfBoard-1-n]-1] == 0 )
						consecutiveMarksOfEachPlayers[chessBoard[i+n][scaleOfBoard-1-n]-1] ++;
					else
						// clear the count of previour player's marks
						consecutiveMarksOfEachPlayers[prevMark-1] = 0;
				}
			}
			// store the current mark
			prevMark = chessBoard[i+n][scaleOfBoard-1-n];
		}
	}

	// check if end in a draw
	bool draw = true;
	for( int i = 0; i != scaleOfBoard; i ++ )
	{
		for( int j = 0; j != scaleOfBoard; j ++ )
		{
			if( chessBoard[i][j] == 0 )
			{
				draw = false;
			}
		}
	}
	if( draw == true )
	{
		gameOver = true;
		whichPlayerWin = 0;
		return gameOver;
	}
	return gameOver;
}

// manage which player's turn
void changePlayer()
{
	if( whichPlayersTurn == howManyPlayers )
		whichPlayersTurn = 1;
	else
		whichPlayersTurn ++;
}
void updateReplay( Replay *(&p) )
{
	if( Replay::head == NULL )
	{
		p = new Replay;
		p->head = p;
	}
	p->next = new Replay;	
	p = p->next;

	p->action[0] = action[0];
	p->action[1] = action[1];
	p->who = whichPlayersTurn;
	if( Replay::ticksFromFile != 0 )
		p->ticks = clock() + p->ticksFromFile;
	else
		p->ticks = clock();
}
void updateReplay()
{
	updateReplay( mReplay );
}

// print the winner
void printTheWinner()
{
	if( isEndLegal() )
	{
		if( whichPlayerWin == -1 )
		{
			cout<<"exit during game....";
		}
		else
		{
			cout<<"\nGame Over"<<endl;
			if( whichPlayerWin == 0 )
				cout<<"end in a draw !! ";
			else
			{
				cout<<"\n \2 \2 \2 \2 \2 \2 \2 \2 \2 \n";
				cout<<"Player "<<whichPlayerWin<<" Win !!"<<endl;
			}
		}
	}
	else
		std::cerr<<"error : illegal end.";
}
bool saveReplay( std::string name )
{
	// save the replay
	std::ofstream mSaveReplay;
	mSaveReplay.close();
	mSaveReplay.clear();
	mSaveReplay.open( name.c_str() );
	if( mSaveReplay )
	{
		mSaveReplay<<"<howManyPlayers>+"<<howManyPlayers<<"-\n";
		mSaveReplay<<"<scaleOfBoard>+"<<scaleOfBoard<<"-\n";
		mSaveReplay<<"<winSequence>+"<<winSequence<<"-\n";
		mSaveReplay<<"<whichPlayerWin>+"<<whichPlayerWin<<"-\n";
		// saving action sequence
		mSaveReplay<<"<actionSequence>+\n";
		mReplay = Replay::head->next;
		while( mReplay != NULL )
		{
			mSaveReplay<<"(";
			mSaveReplay<<"R"<<mReplay->action[0]<<",C"<<mReplay->action[1];
			mSaveReplay<<",W"<<mReplay->who;
			mSaveReplay<<",T"<<mReplay->ticks;
			mSaveReplay<<",)\n";
			// to next
			mReplay = mReplay->next;
		}
		mSaveReplay<<"-\n";
		mSaveReplay.close();
		mReplay = Replay::head;
		delete mReplay;
		mReplay= NULL;
		cout<<"replay saved, exiting......"<<endl;
		return true;
	}
	else
	{
		std::cerr<<"error : can not save replay.";
		return false;
	}
}
void saveReplay()
{
	int choice = 0;
	do
	{
		cout<<"Would you like to save the replay ?\n";
		cout<<"1.Yes, save the replay.\n";
		cout<<"2.No, do not save the replay."<<endl;
		choice = getANumber();
	}while( choice != 1 && choice != 2 );
	if( choice == 1 )
		saveReplay(std::string("replay.dat"));
}
// game loop
void runTheGame()
{
	// initial game settings
	setGame();
	// if game over,quit from the loop
	while( true )
	{
		// print the chess board
		printChessBoard();
		// get input from keyboard
		keyboard();
		if( saveGame == true )
			save();
		if( exitGame == true )
			break;
		// update the chess board
		if( !updateChessBoard())
			std::cerr<<"error : fail to update chess board.";
		// check if the game is over
		if( isGameOver() )
		{
			updateReplay();
			// print the final chess board
			printChessBoard();
			// print the result
			printTheWinner();
			// save replay
			saveReplay();
			break;
		}
		else
		{
			updateReplay();
			// if not, continue the game
			changePlayer();
		}	
	}
}

bool loadReplay( std::string name )
{
	cout<<"\nLoading Replay....";
	std::ifstream mLoadReplay;
	mLoadReplay.close();
	mLoadReplay.clear();
	mLoadReplay.open( name.c_str() );
	if( mLoadReplay )
	{
		// read conditions
		char content;
		std::string type;
		bool readType = false;
		int data = 0;
		bool readData = false;
		char replayType;
		// reading
		while( !mLoadReplay.eof() )
		{
			// keep read from file
			content = mLoadReplay.get();	
			if( content == '<' )
			{// read type
				readType = true;
				// reset type
				type.clear();
			}
			else if( content == '>' )// complete read type
				readType = false;
			else if( readType == true )// add content to string
				type += content;


			if( content == '+' )
			{// read game data
				readData = true;
				// reset data
				data = 0;
				if( type == "actionSequence" )
				{
					mReplay = new Replay;
					Replay::head = mReplay;
				}
			}
			else if( content == '-' )
			{// complete reading game data
				readData = false;
				// store data into game
				if( type == "howManyPlayers" )
					howManyPlayers = data;
				else if( type == "scaleOfBoard" )
					scaleOfBoard = data;
				else if( type == "winSequence" )
					winSequence = data;
				else if( type == "whichPlayerWin" )
					whichPlayerWin = data;
				else if( type == "actionSequence" )
				{
					delete mReplay->next;
					mReplay->next = NULL;
					mReplay = Replay::head;
				}
				else
					std::cerr<<"error : no such type.";
			}
			else if( readData == true )
			{
				if( isdigit( content ) )// integers
					data = 10 * data + ( content - '0');
				if( type == "actionSequence" )
				{
					if( content == '(' )
					{
						mReplay->next = new Replay;
						mReplay = mReplay->next;
					}
					else if( content == 'R'|| content == 'C' || content == 'W' || content == 'T' )
					{
						data = 0;
						replayType = content;
					}
					else if( content == ',' )
					{
						if( replayType == 'R' )
							mReplay->action[0] = data;
						else if( replayType == 'C' )
							mReplay->action[1] = data;
						else if( replayType == 'W' )
							mReplay->who = data;
						else if( replayType == 'T' )
							mReplay->ticks = data;
					}
					/*else if( content == ')' )
					{
						mReplay->next = new Replay;
						mReplay = mReplay->next;
					}*/
				}
			}
		}
		mLoadReplay.close();
		cout<<"\nLoading Complete.";
		return true;
	}
	else
	{
		std::cerr<<"error : can not load replay.";
		return false;
	}
}
bool watchReplay()
{
	if( !loadReplay(std::string("replay.dat")))
		return false;
	initChessBoard( chessBoard, scaleOfBoard );
	cout<<"\n";
	cout<<"===================================\n";
	cout<<"=======   Printing Replay   =======\n";
	cout<<"===================================\n";
	

	mReplay = Replay::head;
	mReplay = mReplay->next;
	while( mReplay != NULL )
	{
		printChessBoard();
		whichPlayersTurn = mReplay->who;
		action[0] = mReplay->action[0];
		action[1] = mReplay->action[1];
		
		cout<<"\nThe player is thinking";
		int i = 0;
		while( clock() < mReplay->ticks )
		{
			i ++;
			if( i > 10000000 )
			{
				i = 0;
				cout<<".";
			}
		}
		
		updateChessBoard();
		mReplay = mReplay->next;
	}
	printChessBoard();
	printTheWinner();

	return true;
}
void run()
{
	int choice = 0;
	// if watch replay
	do
	{
		cout<<"Would you like to watch replay?\n";
		cout<<"1.Yes, watch replay.\n";
		cout<<"2.No, play game.\n";
		choice = getANumber();
	}while( choice != 1 && choice != 2 );
	if( choice == 1 )
	{// watch replay
		if( !watchReplay())
			runTheGame();
	}
	else
	{// play
		runTheGame();
	}
}
// ================================================
// MAIN FUNCTION
// ================================================
// main function
int main()
{
	// print the title
	cout<<"CS570 Xinlei Cao"<<endl;
	cout<<"============================================="<<endl;
	cout<<"====        Tic - Tac - Toe Game         ===="<<endl;
	cout<<"============================================="<<endl;
	
	// run the game loop
	run();

	// pause when debugging
	//std::system( "pause" );
	std::string message;
	cout<<"\n\n\nleave a message and exit............"<<endl;
	cin>>message;
	// end - of - main function
	return 0;
}